from django.contrib import admin
from django.urls import path
from . import views
from jadwal.models import Schedule

admin.site.register(Schedule)

urlpatterns = [
    path('admin/jadwal/schedule', views.get_schedule, name='get_schedule')
]