from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
	path('aboutMe', views.index2, name='index2'),
	path('schedule', views.index3, name='index3'),
	path('help', views.index4, name='index4'),
	path('admin/jadwal/schedule', views.get_schedule, name='get_schedule')
]