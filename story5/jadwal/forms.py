from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    class Meta:
        jadwal = Schedule
        fields = [
            'date',
            'time',
            'name',
            'location',
            'category'
		]

class RawScheduleForm(forms.Form):
    date = forms.DateField()
    time = forms.TimeField()
    name = forms.CharField()
    location = forms.CharField()
    category = forms.CharField()
	
