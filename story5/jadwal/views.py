from django.shortcuts import render
from django.http import HttpResponse
from .forms import ScheduleForm
from .models import Schedule

def index(request):
    return render(request, 'myProfile.html')

def index2(request):
    return render(request, 'aboutMe.html')

def index3(request):
    return render(request, 'schedule.html')

def index4(request):
    return render(request, 'help.html')

def get_schedule(request):
    form = ScheduleForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ScheduleForm()

    context = {
        'form': form
    }
    return render(request, 'schedule.html', context)