from django.db import models

class Schedule(models.Model):
	date = models.DateField()
	time = models.TimeField()
	name = models.TextField(max_length = 100)
	location = models.CharField(max_length = 500)
	category = models.TextField(max_length = 50)